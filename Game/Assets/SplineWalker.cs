﻿using System;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using System.Collections;

public class SplineWalker : MonoBehaviour {

    private List<Assets.ChargeCollectable> chargeLevel = new List<Assets.ChargeCollectable>();
    public List<Assets.ChargeCollectable> ChargeLevel
    {
        get
        {
            return chargeLevel;
        }
        set
        {
            if (this.chargeLevel == null)
            {
                chargeLevel = new List<Assets.ChargeCollectable>();
                chargeLevel.Capacity = maxChargeLevel;
            }
            chargeLevel.Clear();
            value.ForEach(x => chargeLevel.Add(x));

        }
    }

    private int maxChargeLevel = 50;
    public int MaxChargeLevel
    {
        get
        {
            return maxChargeLevel;
        }
    }

    public BezierSpline[] splines;

    private BezierSpline spline;

    public float duration;

    public static bool IsDestroyed {get ; set; }

    public static int location
    {
        get;
        private set;
    }
    public BezierSpline Spline { get { return this.spline; } }
    public bool lookForward;

    public SplineWalkerMode mode;

    private float progress;
    private bool goingForward = true;

    private float trigger = 0.0f;

    public SplineWalker2 friendElectorn;
    private bool canCollectCharges=true;
    private float durationIdle;

    private void Start()
    {
        location = 3;
        spline = splines[location];
        chargeLevel.Capacity = maxChargeLevel;
        GameObject.Find("Object").GetComponent<MeshRenderer>().enabled = false;
    }
    #region Charge Managment and OnTrigger
    public double CalculateAverageChargeLevel()
    {

        return 0.5+(this.ChargeLevel.Count + friendElectorn.ChargeLevel.Count) / 2.0;
    }
    public void ChargeTransfer()
    {
        
        int averageChargeLevel = (int) CalculateAverageChargeLevel();
        Debug.Log(averageChargeLevel);

        ChargeLevel.Clear();
        friendElectorn.ChargeLevel.Clear();

        for (int i = 0; i < averageChargeLevel; i++)
        {
            ChargeLevel.Add(new ChargeCollectable());
            friendElectorn.ChargeLevel.Add(new ChargeCollectable());
        }

    }
    public void IncreaseChargeLevel()
    {
        if (chargeLevel.Count == maxChargeLevel)
            throw new System.ArgumentOutOfRangeException();
        else
        {
            chargeLevel.Add(new Assets.ChargeCollectable());
            Debug.Log("Walker1 "+"Charge Level = " + this.chargeLevel.Count);
        }
    }
    public void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("ChargeCollectable") && canCollectCharges)
        {
            try
            {
                IncreaseChargeLevel();
                transform.localScale = new Vector3(transform.localScale.x + 0.05f, transform.localScale.y + 0.05f, transform.localScale.z + 0.05f);
                StartCoroutine(AD());
            }
            catch(System.ArgumentOutOfRangeException e)
            {
                Debug.Log("Exceeded Max Charge Level KABOOM");
                IsDestroyed = true;
                Destroy(this);
                return;
            }
            
            Destroy(c.gameObject);
        }
        if (c.CompareTag("Player2"))
            ChargeTransfer();
        if(c.CompareTag("LightingBolt") && this.ChargeLevel.Count != 0)
        {
            if (friendElectorn.Spline == this.spline)
            {
                this.chargeLevel.RemoveAt(this.ChargeLevel.Count - 1);
                Debug.Log("Removing one charge present Count is " + this.chargeLevel.Count);
            }
            else
            {
                Debug.Log("Killed By LightingBolt R.I.P");
                IsDestroyed = true;
                Destroy(this);
                Destroy(friendElectorn);
            }
        }
        
    }
    #endregion
    #region Update
    private void Update () {

        durationIdle -= Time.deltaTime;

        if (location == SplineWalker2.location)
        {
            canCollectCharges = false;

        }
        if (location != SplineWalker2.location)
        {
            canCollectCharges = true;

        }

        if (durationIdle<=0)
        {
            
        }
        trigger = Input.GetAxis("Trigger");

        if (trigger < 0)
        {
            spline = splines[MarkerWalker.location];
            location = MarkerWalker.location;
        }

        if (goingForward) {
			progress += Time.deltaTime / duration;
			if (progress > 1f) {
				if (mode == SplineWalkerMode.Once) {
					progress = 1f;
				}
				else if (mode == SplineWalkerMode.Loop) {
					progress -= 1f;
				}
				else {
					progress = 2f - progress;
					goingForward = false;
				}
			}
		}
		else {
			progress -= Time.deltaTime / duration;
			if (progress < 0f) {
				progress = -progress;
				goingForward = true;
			}
		}
       
        Vector3 position = spline.GetPoint(progress);
		transform.localPosition = position;
		if (lookForward) {
			transform.LookAt(position + spline.GetDirection(progress));
		}
	}
    #endregion

    private IEnumerator AD()
    {
        GameObject.Find("Object").GetComponent<MeshRenderer>().enabled = true;
        yield return new WaitForSeconds(0.3f);
        GameObject.Find("Object").GetComponent<MeshRenderer>().enabled = false;
    }
}
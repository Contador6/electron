﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Assets.Scripts
{
    [XmlType("Player")]
    public class Player 
    {


        private string _name;
        private int _points;
        private DateTime _date;
        [XmlElement("name")]
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        [XmlElement("points")]
        public int points
        {
            get { return _points; }
            set { _points = value; }
        }

        [XmlElement("date")]
        public DateTime date
        {
            get { return _date; }
            set { _date = value; }
        }

        public Player(string name, int points, DateTime date)
        {
            this._name = name;
            this._points = points;
            this._date = date;
        }

        public Player()
        {

        }

    }
}

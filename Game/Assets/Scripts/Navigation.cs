﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Navigation : MonoBehaviour {


    public void Start()
    {
        LoadPodklad();
    }

    private void LoadPodklad()
    {
        SceneManager.LoadScene("Podklad", LoadSceneMode.Additive);
    }

    public void Newgame()
    {
        GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.y-500.0f, GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Information").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Information").GetComponent<RectTransform>().localPosition.x,
           GameObject.Find("Information").GetComponent<RectTransform>().localPosition.y - 500.0f, GameObject.Find("Information").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Quit").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.x,
          GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.y - 500.0f, GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.x,
           GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.y+500.0f, GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.y+500.0f, GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Return").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Return").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("Return").GetComponent<RectTransform>().localPosition.y + 500.0f, GameObject.Find("Return").GetComponent<RectTransform>().localPosition.z);
    }

    public void PlayShooter()
    {
        SceneManager.LoadScene("OnePlayerShooter");
    }

    public void PlayCollecter()
    {
        SceneManager.LoadScene("OnePlayer");
    }

    public void returnToMenu()
    {
        OnePlayer.scoredPoints = 0;
        SceneManager.LoadScene("Menu 3D");
        
    }

    public void showResults()
    {
        SceneManager.LoadScene("Results");
    }

    public void CancelGame()
    {
        GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.y + 500.0f, GameObject.Find("Newgame").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Information").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Information").GetComponent<RectTransform>().localPosition.x,
           GameObject.Find("Information").GetComponent<RectTransform>().localPosition.y + 500.0f, GameObject.Find("Information").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Quit").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.x,
          GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.y + 500.0f, GameObject.Find("Quit").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.x,
           GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.y - 500.0f, GameObject.Find("PlayCollecter").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.y - 500.0f, GameObject.Find("PlayShooter").GetComponent<RectTransform>().localPosition.z);

        GameObject.Find("Return").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Return").GetComponent<RectTransform>().localPosition.x,
            GameObject.Find("Return").GetComponent<RectTransform>().localPosition.y - 500.0f, GameObject.Find("Return").GetComponent<RectTransform>().localPosition.z);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

    public void LoadInstructionAvoiding()
    {
        SceneManager.LoadScene("IntroductionCollecter2");
    }

    public void LoadInstructionSteering()
    {
        SceneManager.LoadScene("IntroductionCollecter");
    }

    public void LoadInstructionEliminate()
    {
        SceneManager.LoadScene("IntroductionCollecter3");
    }

    public void LoadInstructionDischarging()
    {
        SceneManager.LoadScene("IntroductionCollecter4");
    }

    public void LoadInstructionPortal()
    {
        SceneManager.LoadScene("IntroductionCollecter5");
    }

    public void LoadInstructionSteering2()
    {
        SceneManager.LoadScene("IntroductionShooter");
    }

    public void LoadInstructionBonus()
    {
        SceneManager.LoadScene("IntroductionShooter2");
    }

}

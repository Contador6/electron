﻿using UnityEngine;
using Assets.Scripts;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using System.Xml.Serialization;

public class ShowResults : MonoBehaviour {

    private List<Player> lista;

    private List<string> tagsRecords = new List<string>() { "R1","R2","R3","R4","R5"};

	// Use this for initialization
	public void Start () { 
        lista = Serialization.OnlyRead();

        LoadResults();
	}

    private void LoadResults(){

        for (int i = 0; i < tagsRecords.Count && i < lista.Count; i++)
        {
            GameObject.FindGameObjectWithTag(tagsRecords[i]).GetComponentInChildren<Text>().text = lista[i].name + " " + lista[i].points;
        }
    }

    public void ClearResults(){
        try
        {
            File.Delete("Results.xml");
        }
        catch(FileNotFoundException e)
        {

        }

        TextWriter tw = new StreamWriter("Results.xml");
        XmlSerializer srr = new XmlSerializer(typeof(Serialization.Results));
        srr.Serialize(tw, new Serialization.Results());
        tw.Close();

        lista.Clear();

        for (int i = 0; i < tagsRecords.Count; i++)
        {
            GameObject.FindGameObjectWithTag(tagsRecords[i]).GetComponentInChildren<Text>().text = "";
        }

    }
	
}

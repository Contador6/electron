﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts;
using UnityEngine.SceneManagement;
using System.Linq;

public class Serialization : MonoBehaviour {

    // Use this for initialization
    public void Save()
    {
        GameObject gameObject = GameObject.Find("InputField");
        InputField input = gameObject.GetComponentInChildren<InputField>();
        Player player = new Player(input.text, OnePlayer.scoredPoints, DateTime.Now);
        OnePlayer.scoredPoints = 0;
        List<Player> lista=null;
        Results results = null;

        try
        {
            results = Read(results, lista);
            lista = results.player;
        }
        catch (FileNotFoundException e)
        {
            lista = new List<Player>() {};
        }

        lista.Add(player);


        lista = lista.OrderByDescending(o => o.points).ToList();

        foreach (Player pp in lista)
        {
            Console.WriteLine(pp.name + pp.points);
        }

        TextWriter tw = new StreamWriter("Results.xml");
        XmlSerializer srr = new XmlSerializer(typeof(Results));
        srr.Serialize(tw, new Results(lista));
        tw.Close();

        SceneManager.LoadScene("Results");
    }

    private Results Read(Results results,List<Player> lista) 
    {
        TextReader tr = new StreamReader("Results.xml");
        XmlSerializer sr = new XmlSerializer(typeof(Results));
        results = (Results)sr.Deserialize(tr);
        tr.Close();
        lista = results.player;
        return results;
    }

    public static List<Player> OnlyRead()
    {
        Results results;
        try
        {
            TextReader tr = new StreamReader("Results.xml");
            XmlSerializer sr = new XmlSerializer(typeof(Results));
            results= (Results)sr.Deserialize(tr);
            tr.Close();
        }
        catch(FileNotFoundException e)
        {
            return new List<Player>();
        }

        return results.player;
    }

    [XmlRoot("Results")]
    public class Results
    {

        private List<Player> _player;

        [XmlElement("player")]
        public List<Player> player
        {
            get { return _player; }
            set { _player = value; }
        }

        public Results(List<Player> player)
        {
            this._player = player;
        }

        public Results()
        {

        }
    }

   
}

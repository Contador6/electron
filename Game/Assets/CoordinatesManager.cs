﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class CoordinatesManager
{
    private static short[] tableInversion = new short[2] { 1, 1 };

    private static List<String> description = new List<string> { "LeftUp", "RightUp", "LeftDown", "RightDown" };

    private static int[,] table = new int[,] { { 0, 1 }, { 2, 3 } };

    public static int checkMovement(Vector2 actual, Vector2 target)
    {
        int result;
        try
        {
            result = table[(int)(actual.x + target.x), (int)(actual.y + target.y)];
        }
        catch (IndexOutOfRangeException e)
        {
            return table[(int)actual.x, (int)actual.y];
        }
        return result;
    }


    public static Vector2 checkCoordinates(int location)
    {
        for (int y = 0; y < table.GetLength(0); y++)
        {
            for (int x = 0; x < table.GetLength(1); x++)
            {
                if (location == table[y, x])
                {
                    return new Vector2(y, x);
                }
            }
        }
        return new Vector2(0, 0);
    }

    public static void resetTableInversion()
    {
        tableInversion[0] = 1;
        tableInversion[1] = 1;
    }

    public static string getDescription(int index)
    {
        return description[index];
    }

    public static void inverseRight()
    {
        tableInversion[1] *= (-1);
    }

    public static void inverseLeft()
    {
        tableInversion[0] *= (-1);
    }

    public static short getInversion(int location)
    {
        return tableInversion[location];
    }

}


﻿using UnityEngine;


public class RaycastShooting : MonoBehaviour {

    public Transform effect;

    public Transform shoot;

    private float trigger = 0.0f;

    public Texture2D []textures;

    public AudioClip pistolShot;

    public AudioClip shieldShot;

    private AudioSource audio;

    private AudioSource audio1;

    private Texture2D actualTexture;

    private Rect position;

    public static Vector3 positionShoot;

    public static Vector3 target;

    public static Quaternion rotation;

    public static Vector3 direction;

    public Transform cameraController;


    void Start()
    {
        //Cursor.visible = false;
        position = new Rect((Screen.width - textures[0].width) / 2,
                    (Screen.height - textures[0].height) / 2,
                    textures[0].width,
                    textures[0].height);
        actualTexture = textures[0];

        audio = gameObject.AddComponent<AudioSource>();
        audio1 = gameObject.AddComponent<AudioSource>();
        audio.clip = pistolShot;
        audio1.clip = shieldShot;
    }

    void OnGUI()
    {
        GUI.DrawTexture(position, actualTexture);
    }

    void OnEnable()
    {
        EventManager.StartListening("ActivateDanger", ActivateDanger);
        EventManager.StartListening("DeactivateDanger",DeactivateDanger);
    }

    public void ActivateDanger()
    {
        actualTexture = textures[0];
    }

    public void DeactivateDanger()
    {
        actualTexture = textures[1];
    }

    void Update()
    {

        transform.position = Camera.main.transform.localPosition;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0.0f));
        if (!QTTriggerShooter.disableSteering)
            trigger = Input.GetAxis("Trigger");
        if (trigger <= -0.9f)
        {
            
            Transform clone; 
            Transform item;
            positionShoot = cameraController.transform.position;
            rotation = cameraController.transform.rotation;
            direction = ray.direction;
            clone = shoot;
            item = Instantiate(clone) as Transform;

            if (Physics.Raycast(ray, out hit, 50.0f))
            {
                

                if (hit.collider.CompareTag("ChargeCollectable"))
                {
                    audio.Play();
                    Destroy(hit.collider.gameObject);
                    if(OnePlayerShooter.shieldPoints<2)
                    {
                        OnePlayerShooter.shieldPoints++;
                        EventManager.TriggerEvent("AddShield");
                    }
                    
                    EventManager.TriggerEvent("IncreaseChargeLevel");
                }

                else if (hit.collider.CompareTag("ActivateSpawn"))
                {
                    audio1.Play();
                    OnePlayer.scoredPoints += 100;
                    EventManager.TriggerEvent("SpawnCharges");
                    Destroy(hit.collider.gameObject);
                    Destroy(item.gameObject, 0.5f);
                }

              
               
            }
            Destroy(item.gameObject, 0.2f);
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class RightController : MonoBehaviour {

    public BezierSpline spline;

    public OnePlayer player;

    public SplineWalkerMode mode;

    public float progress
    {
        get;
        set;
    }

    public bool lookForward;

    public float duration;

    private bool goingForward = true;

    private float timer = 0.5f;

    void Start()
    {
        
    }

    public void OnTriggerEnter(Collider c)
    {
        if ((c.CompareTag(CoordinatesManager.getDescription(2)) && (timer <= 0.0f)))
        {
            timer = 1.0f;
            CoordinatesManager.inverseRight();
        }
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (goingForward)
        {
            progress = player.progress;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;

                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }


}

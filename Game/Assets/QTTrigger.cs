﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTTrigger : MonoBehaviour
{
    //private Image timerImage;
    private bool fill;
    private float currentAmount;
    private float speed = 1000;

    private Canvas canvas;
    private Text text;

    public static bool StartQTE;
    public static bool Ban;
    
    private float countTimer = 0.1f;

    public enum QTState { Ready, Ongoing, Done };
    public QTState qtState = QTState.Ready;

    public enum QTResponse { Null, Success, Fail };
    public QTResponse qtResponse = QTResponse.Null;

    public KeyCode[] options;

    public string PlayerTag = "Player";

    public Dictionary<KeyCode, string> KeyText;

    private KeyCode randomizedButton;

    private void Awake()
    {
        options = new KeyCode[4]
        {
            /*KeyCode.A,
            KeyCode.B,
            KeyCode.X,
            KeyCode.Y*/
            KeyCode.JoystickButton0,
            KeyCode.JoystickButton1,
            KeyCode.JoystickButton2,
            KeyCode.JoystickButton3
        };
        KeyText = new Dictionary<KeyCode, string>();
        KeyText.Add(options[0], "A");
        KeyText.Add(options[1], "B");
        KeyText.Add(options[2], "X");
        KeyText.Add(options[3], "Y");
        canvas = GameObject.Find("QTE_Canvas").GetComponent<Canvas>();
        //timerImage = canvas.GetComponentInChildren<Image>();
        text = canvas.GetComponentInChildren<Text>();
    }

    private void Start()
    {
        StartQTE = false;
        Ban = false;
        fill = false;
        turnPanelOff();
    }

    private void randomizeButton()
    {
        int r;
        System.Random random = new System.Random();
        r = random.Next(0, options.Length);
        randomizedButton = options[r];
    }

    public void turnPanelOn()
    {
        canvas.enabled = true;
    }

    public void turnPanelOff()
    {
        canvas.enabled = false;
    }

    public void changeText(KeyCode code)
    {
        text.text = KeyText[code];
    }

    private void Update()
    {
        if (fill)
        {
            if (currentAmount > 0)
            {
                currentAmount -= speed * Time.deltaTime;
            }
            //timerImage.fillAmount = currentAmount / 100;
            text.fontSize = (int) currentAmount;
        }

        if (StartQTE)
        {
            if (qtState == QTState.Ongoing)
            {
                if (Input.GetKeyDown(randomizedButton))
                {
                    qtState = QTState.Done;
                    qtResponse = QTResponse.Success;
                    StopCoroutine(StateChange());
                    turnPanelOff();
                    Time.timeScale = OnePlayer.currentSpeed;
                    Debug.Log("dobrze wcisnąłem");
                    GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Dobrze";
                    GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
                    OnePlayer.visitedBolts++;
                    EventManager.TriggerEvent("showSuccess");
                    StartCoroutine(OnePlayer.turnOffInfo());
                }
                else if (Input.anyKeyDown)
                {
                    qtState = QTState.Done;
                    qtResponse = QTResponse.Fail;
                    StopCoroutine(StateChange());
                    turnPanelOff();
                    Time.timeScale = OnePlayer.currentSpeed;
                    GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Nie udało się, komora pusta";
                    GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
                    Debug.Log("źle wcisnąłem");
                    StartCoroutine(OnePlayer.turnOffInfo());
                }
            }
        }
        if (qtResponse == QTResponse.Fail && qtState == QTState.Done)
            OnePlayer.lightningBoltFailure = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (StartQTE)
        {
            if (qtState == QTState.Ready && other.tag == PlayerTag)
            {
                randomizeButton();
                Debug.Log("Wlazłem w piorun");
                Ban = true;
                Time.timeScale = 0.1f;

                StartCoroutine(StateChange());
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (StartQTE)
        {
            if (other.tag == PlayerTag)
            {
                qtState = QTState.Ready;
                Ban = false;
                StopCoroutine(StateChange());
                fill = false;
            }
        }
    }

    private IEnumerator StateChange()
    {
        changeText(randomizedButton);
        currentAmount = 150;
        fill = true;
        turnPanelOn();
        qtState = QTState.Ongoing;
        yield return new WaitForSeconds(countTimer);
        // If the timer is over and the event isn't over? Fix it! because most likely they failed.
        if (qtState == QTState.Ongoing)
        {
            turnPanelOff();
            Time.timeScale = OnePlayer.currentSpeed;
            fill = false;
            qtState = QTState.Done;
            qtResponse = QTResponse.Fail;
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Nie udało się, komora pusta";
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
            StartCoroutine(OnePlayer.turnOffInfo());
        }
    }
}

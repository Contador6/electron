﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TimeManagment : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
    void OnTriggerEnter(Collider colider)
    {
        if (colider.CompareTag("ChargeCollectable"))
        {
            
            EventManager.TriggerEvent("IncreseTimeRemaining");
        }
        if (colider.CompareTag("Discharger"))
        {
            EventManager.TriggerEvent("AddBonusTime");
        }
        if (colider.CompareTag("Portal"))
        {
            EventManager.TriggerEvent("SaveRemainingTimeToGameController");
        }

    }
    void OnTriggerExit(Collider colider)
    {
        if (colider.CompareTag("LightningBolt"))
        {

            EventManager.TriggerEvent("DecreseTimeRemaining");
            
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}

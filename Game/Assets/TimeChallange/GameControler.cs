﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class GameControler : MonoBehaviour {

    public static GameControler gameControllerInstance;
    private float timeChallangeTimeRemaining;

    public float TimeRemaining
    {
        get { return timeChallangeTimeRemaining; }
        set { timeChallangeTimeRemaining = value; }
    }
    private int score;

    public int Score
    {
        get { return score; }
        set { score = value; }
    }


    void OnEnable()
    {
        EventManager.StartListening("TimeElapsed", TimeElapsed);
        EventManager.StartListening("GameOver", GameOver);

    }

    private void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    void OnDisable()
    {
        EventManager.StopListening("TimeElapsed", TimeElapsed);
        EventManager.StopListening("GameOver", GameOver);

    }
    public void LoadNewLevel()
    {
        if(SceneManager.GetActiveScene().name == "OnePlayer")
        {
            SceneManager.LoadSceneAsync("TimeChallangeScene");
        }
        else
        {
            SceneManager.LoadSceneAsync("OnePlayer");
        }
        
    }
    private void TimeElapsed()
    {
        SceneManager.LoadScene("GameOver");
    }

    void Awake()
    {
        Debug.Log("Score Points " + Score);
        if (gameControllerInstance == null)
        {
            DontDestroyOnLoad(gameObject);
            gameControllerInstance = this;
        }
        else if (gameControllerInstance != this)
        {
            Destroy(gameObject);
        }
    }
	 
	// Update is called once per frame
	void Update () {
	
	}
    void Start()
    {
        
    }
}

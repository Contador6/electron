﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

 public float timeRemaining;

    void OnEnable()
    {
        EventManager.StartListening("IncreseTimeRemaining", IncreseTimeRemaining);
        EventManager.StartListening("AddBonusTime", AddBonusTime);
        EventManager.StartListening("DecreaseTimeRemaining", DecreaseTimeRemaining);
        EventManager.StartListening("SaveRemainingTimeToGameController", SaveRemainingTimeToGameController);
    }

    private void SaveRemainingTimeToGameController()
    {
        GameControler.gameControllerInstance.LoadNewLevel();
    }

    void OnDisable()
    {
        EventManager.StopListening("IncreseTimeRemaining", IncreseTimeRemaining);
        EventManager.StopListening("AddBonusTime", AddBonusTime);
        EventManager.StopListening("DecreaseTimeRemaining", DecreaseTimeRemaining);
    }
    void Start()
    {
        //if(GameControler.gameControllerInstance.TimeRemaining != 0)
        //{
        //    timeRemaining = GameControler.gameControllerInstance.TimeRemaining;
        //}
        InvokeRepeating("DecreaseTimeRemaining", 1.0f, 1.0f);
    }

    void Update()
    {
        
        if (timeRemaining <= 0)
        {
            // EventManager.TriggerEvent("TimeElapsed");
            SceneManager.LoadScene("GameOver");
        }


       





    }

    void DecreaseTimeRemaining()
    {
        timeRemaining--;
        Debug.Log(timeRemaining);
        GameObject.Find("TimerCanvas").GetComponent<Canvas>().GetComponentInChildren<Text>().text = timeRemaining.ToString();
    }

    private void IncreseTimeRemaining()
    {
        timeRemaining +=2f;
        Debug.Log("time Increased + "+2);
        
    }
    private void AddBonusTime()
    {
        timeRemaining += 2f;
        Debug.Log("Bonus Time + "+2);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gui : MonoBehaviour
{
    public SplineWalker walker;
    public SplineWalker2 walker2;


    private GUIStyle style1 = null;
    private GUIStyle style2 = null;
    private GUIStyle style3 = null;
    private GUIStyle style4 = null;
    private Texture2D texture;
    public TextAsset imageAsset;

    private void Start()
    {
        //texture = new Texture2D(256, 32);
        //byte[] bytes = System.IO.File.ReadAllBytes("Resources/energy");
        //texture.LoadImage(imageAsset.bytes);
        //GetComponent<Renderer>().material.mainTexture = texture;
    }

    private void InitStyles()
    {
        if (style1 == null || style2 == null || style3 == null || style4 == null)
        {
            style1 = new GUIStyle(GUI.skin.box);
            style1.normal.background = MakeTex(2, 2, new Color(1.0f, 0.20f, 0.0f, 0.3f));
            style2 = new GUIStyle(GUI.skin.box);
            style2.normal.background = MakeTex(2, 2, new Color(1.0f, 0.20f, 0.0f, 1.0f));
            style3 = new GUIStyle(GUI.skin.box);
            style3.normal.background = MakeTex(2, 2, new Color(0.0f, 0.00f, 1.0f, 0.3f));
            style4 = new GUIStyle(GUI.skin.box);
            style4.normal.background = MakeTex(2, 2, new Color(0.0f, 0.00f, 1.0f, 1.0f));
        }
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    void OnGUI()
    {
        InitStyles();
        //GUI.Box(new Rect(0, 0, 100, 100), "Hello", style1);
        // Create one Group to contain both images
        // Adjust the first 2 coordinates to place it somewhere else on-screen
        GUI.BeginGroup(new Rect(20, Screen.height - 276, 32, 256));

        // Draw the background image
        GUI.Box(new Rect(0, 0, 32, 256), "", style1);

        // Create a second Group which will be clipped
        // We want to clip the image and not scale it, which is why we need the second Group
        GUI.BeginGroup(new Rect(0, 0, 32, walker2.ChargeLevel.Count * 256 / walker2.MaxChargeLevel));

        // Draw the foreground image
        GUI.Box(new Rect(0, 0, 32, 256), "", style2);

        // End both Groups
        GUI.EndGroup();
        GUI.EndGroup();

        GUI.BeginGroup(new Rect(Screen.width - 52, Screen.height - 276, 32, 256));
        GUI.Box(new Rect(0, 0, 32, 256), "", style3);
        GUI.BeginGroup(new Rect(0, 0, 32, walker.ChargeLevel.Count * 256 / walker.MaxChargeLevel));
        GUI.Box(new Rect(0, 0, 32, 256), "", style4);
        GUI.EndGroup();
        GUI.EndGroup();
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class OnePlayer : MonoBehaviour {

    public GameObject Bullet;

    public BezierSpline[] splines;

    private BezierSpline spline;

    public MarkerOnePlayer marker;

    public CameraController camera;

    public LeftController left;

    public RightController right;

    public AbstractSpawner spawner;

    public Material[] materials;

    public Transform effect;

    private Queue<GameObject> shotsObjects;

    private List<GameObject> chargeInfo;
    private bool isDead = false;

    private List<Assets.ChargeCollectable> chargeLevel = new List<Assets.ChargeCollectable>();
    public List<Assets.ChargeCollectable> ChargeLevel
    {
        get
        {
            return chargeLevel;
        }
        set
        {
            if (this.chargeLevel == null)
            {
                chargeLevel = new List<Assets.ChargeCollectable>();
                chargeLevel.Capacity = maxChargeLevel;
            }
            chargeLevel.Clear();
            value.ForEach(x => chargeLevel.Add(x));

        }
    }

    private int maxChargeLevel = 10;
    public int MaxChargeLevel
    {
        get
        {
            return maxChargeLevel;
        }
    }

    private string[] chargesTags = new string[] {"Ch1","Ch2","Ch3","Ch4","Ch5","Ch6","Ch7","Ch8","Ch9","Ch10"};

    public static bool hit = false;

    private short possibleShots = 0;

    public static bool shoot = true;

    public static volatile bool lightningBoltFailure;

    public static bool firstHalf = true;

    public static bool secondHalf = false;

    public float duration;

    public static float currentSpeed=1.0f;

    private bool gamePaused;

    private int percentageDistance = 60;

    public bool lookForward;

    public static int visitedBolts
    {
        get;
        set;
    }

    public static int location
    {
        get;
        private set;
    }

    public SplineWalkerMode mode;
    public float progress
    {
        get;
        private set;
    }
    private bool goingForward = true;

    private float trigger = 0.0f;

    private bool canCollectCharges = true;

    public static int scoredPoints
    {
        get;
        set;
    }

    private void Start()
    {
        location = 3;
        spline = splines[location];
        chargeLevel.Capacity = maxChargeLevel;
        scoredPoints = 0;
        gamePaused = false;
        QTTrigger.StartQTE = false;
        GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;
        GameObject.FindGameObjectWithTag("CompletedChallenge").GetComponent<ParticleSystem>().enableEmission = false;
        GetComponent<Renderer>().material = materials[0];
        possibleShots = 3;
        progress = 0.020f;
        shotsObjects = new Queue<GameObject>(GameObject.FindGameObjectsWithTag("InfoBullet"));
        visitedBolts = 0;
        lightningBoltFailure = false;
        if(GameControler.gameControllerInstance.Score !=0)
        {
            scoredPoints += GameControler.gameControllerInstance.Score;
        }
        
        chargeInfo = new List<GameObject>();
    

        foreach(string tag in chargesTags)
        {
            chargeInfo.Add(GameObject.FindGameObjectWithTag(tag));
        }
        
    }
    #region Charge Managment and OnTrigger


    public void IncreaseChargeLevel()
    {
        if (chargeLevel.Count == maxChargeLevel)
            throw new System.ArgumentOutOfRangeException();
        else
        {
            chargeLevel.Add(new Assets.ChargeCollectable());
            if (chargeLevel.Count == maxChargeLevel)
            {
                EventManager.TriggerEvent("SpawnDischarger");
                GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = false;
                GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = true;
                GetComponent<Renderer>().material = materials[1];

            }

            chargeInfo[chargeLevel.Count - 1].GetComponent<Renderer>().enabled = true;


            GuiOnePlayer.blue -= 1.0f / MaxChargeLevel;
            GuiOnePlayer.red += 1.0f / MaxChargeLevel;

       
            int result = chargeLevel.Count*100 / (maxChargeLevel);
            
            if(chargeLevel.Count+1==maxChargeLevel)
            {
                GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Jeden ładunek do pełnego zebrania";
            }
            else
            {
                GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = result.ToString() + "% komory";
            }
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
            StartCoroutine(turnOffInfo());
        }
    }


    public void EmptyInfoCharges()
    {
        foreach(GameObject gg in chargeInfo)
        {
            gg.GetComponent<Renderer>().enabled = false;
        }
    }


    public static IEnumerator turnOffInfo()
    {
        yield return new WaitForSeconds(2);
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = false;
    }

    public void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("ChargeCollectable") && canCollectCharges)
        {
            try
            {
                IncreaseChargeLevel();

            }
            catch (System.ArgumentOutOfRangeException e)
            {
                GuiOnePlayer.blue = 1.0f;
                GuiOnePlayer.red = 0.0f;
                scoredPoints += (ChargeLevel.Count * 10);
                CoordinatesManager.resetTableInversion();
                QTTrigger.StartQTE = false;
                QTTrigger.Ban = false;
                isDead = true;
                
                Destroy(this);
                
                return;
            }

            Destroy(c.gameObject);
        }
        if (c.CompareTag("Discharger"))
        {
            EventManager.TriggerEvent("SpawnPortal");
            EmptyInfoCharges();
            int result;
            if (ChargeLevel.Count == MaxChargeLevel)
                scoredPoints += 100;
            scoredPoints += ChargeLevel.Count;
            scoredPoints += possibleShots * 100;
            scoredPoints += visitedBolts * 10;
            visitedBolts = 0;
            //maxChargeLevel++;
            result = 100 + possibleShots * 100 + visitedBolts * 10 + ChargeLevel.Count;
            chargeLevel.Clear();
            GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = true;
            GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;
            GetComponent<Renderer>().material = materials[0];
            possibleShots = 3;
            SpeedUp();
            foreach (GameObject gg in GameObject.FindGameObjectsWithTag("Discharger"))
            {
                Destroy(gg);
            }

            shotsObjects.Clear();
            foreach (GameObject gg in GameObject.FindGameObjectsWithTag("InfoBullet"))
            {
                shotsObjects.Enqueue(gg);
                gg.GetComponentInChildren<ParticleSystem>().enableEmission = true;
                gg.GetComponent<Renderer>().enabled = false;
            }

            GuiOnePlayer.blue = 1.0f;
            GuiOnePlayer.red = 0.0f;

            

         
                GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Komora rozładowana zebrano "+ result.ToString() +" pkt";

            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
            StartCoroutine(turnOffInfo());

        }

    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.CompareTag("Portal"))
        {
            if (ChargeLevel.Count == MaxChargeLevel)
                scoredPoints += 100;
            scoredPoints += (ChargeLevel.Count * 10);
            int tmp = 0;
            tmp += scoredPoints;
            GameControler.gameControllerInstance.Score += tmp;
            Debug.Log("PORTAL!!!");
            GameControler.gameControllerInstance.LoadNewLevel();
            
        }
    }
    #endregion
    #region Update
    private void SpeedUp()
    {
        OnePlayer.currentSpeed += 0.1f;
        Time.timeScale = OnePlayer.currentSpeed;
    }
    private void Update()
    {

        if((int)(progress * 100)==60&& (int)(progress * 100)!=percentageDistance)
        {
            percentageDistance = 60;
            firstHalf = true;
        }

        if ((int)(progress * 100) == 20 && (int)(progress * 100) != percentageDistance)
        {
            percentageDistance = 20;
            secondHalf = true;
        }


        QTTrigger.StartQTE = true;
        if (lightningBoltFailure)
        {
            ChargeLevel.Clear();
            lightningBoltFailure = false;
            GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = true;
            GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;
            GetComponent<Renderer>().material = materials[0];
            EmptyInfoCharges();
            foreach (GameObject gg in GameObject.FindGameObjectsWithTag("Discharger"))
            {
                Destroy(gg);
            }
        }

        if (Input.GetKeyDown("escape"))
        {
            if (ChargeLevel.Count == MaxChargeLevel)
                scoredPoints += 100;
            scoredPoints += (ChargeLevel.Count * 10);
            scoredPoints = 0;
            CoordinatesManager.resetTableInversion();
            QTTrigger.StartQTE = false;
            QTTrigger.Ban = false;
            isDead = true;
            Destroy(this);
            return;
            SceneManager.LoadScene("Menu 3D");
        }

        else if (Input.GetKeyDown(KeyCode.P))
        {
            gamePaused = !gamePaused;
            if (gamePaused)
                Time.timeScale = 0.0f;
            else
                Time.timeScale = 1.0f;
        }

        if (!QTTrigger.Ban)
            trigger = Input.GetAxis("Trigger");


        if (trigger <= -0.9f && shoot && possibleShots >= 1)
        {
            shoot = false;
            possibleShots--;
            GameObject.FindGameObjectWithTag("BulletFlame").GetComponent<ParticleSystem>().enableEmission = true;
            GameObject.FindGameObjectWithTag("Bullet").GetComponent<BoxCollider>().enabled = true;
            StartCoroutine(decativateBullet());
            GameObject hh = shotsObjects.Dequeue();
            hh.GetComponent<Renderer>().enabled = false;
            hh.GetComponentInChildren<ParticleSystem>().enableEmission = false;
        }

        if (Input.GetButton("XButton") && !QTTrigger.Ban)
        {
            spline = splines[MarkerOnePlayer.location];
            location = MarkerOnePlayer.location;
            
        }

        if (goingForward)
        {

            progress += Time.deltaTime / duration;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }
    #endregion
    void OnDestroy()
    {
        QTTrigger.StartQTE = false;
        QTTrigger.Ban = false;
        if (ChargeLevel.Count == MaxChargeLevel)
            scoredPoints += 100;
        scoredPoints += (ChargeLevel.Count * 10);
        //scoredPoints = 0;
        GuiOnePlayer.blue = 1.0f;
        GuiOnePlayer.red = 0.0f;
        CoordinatesManager.resetTableInversion();
        
        if(isDead)
           SceneManager.LoadScene("GameOver");



    }

    private IEnumerator decativateBullet()
    {
        yield return new WaitForSeconds(2);
        GameObject.FindGameObjectWithTag("Bullet").GetComponent<BoxCollider>().enabled = false;
        GameObject.FindGameObjectWithTag("BulletFlame").GetComponent<ParticleSystem>().enableEmission = false;
        OnePlayer.shoot = true;
    }


    public void showSuccess()
    {
        GameObject.FindGameObjectWithTag("CompletedChallenge").GetComponent<ParticleSystem>().enableEmission = true;
        StartCoroutine(startCountingSuccess());
    }

    void OnEnable()
    {
        EventManager.StartListening("showSuccess", showSuccess); 
    }


    public IEnumerator startCountingSuccess()
    {
        yield return new WaitForSeconds(2);
        GameObject.FindGameObjectWithTag("CompletedChallenge").GetComponent<ParticleSystem>().enableEmission = false;
    }



}

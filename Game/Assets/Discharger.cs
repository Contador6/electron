﻿using UnityEngine;
using System.Collections;

public class Discharger : MonoBehaviour {

	void Start () {
	
	}
	
	void Update () {
	
	}

    public void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("LightningBolt") || collider.CompareTag("ChargeCollectable"))
        {
            Destroy(collider.gameObject);
        }
    }
}

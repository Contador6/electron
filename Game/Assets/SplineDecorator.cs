﻿using System.Collections.Generic;
using UnityEngine;

public class SplineDecorator : MonoBehaviour {

	public BezierSpline spline;

	public Transform[] items;

    public static List<Vector3> poss = new List<Vector3>();

    private float stepSize;

    public int frequency;

    public bool lookForward;


    private void Awake()
    {
        items[0].transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));
        if (frequency <= 0 || items == null || items.Length == 0)
        {
            return;
        }
        stepSize = frequency * items.Length;

        if (spline.Loop || stepSize == 1)
        {
            stepSize = 1f / stepSize;
        }
        else {
            stepSize = 1f / (stepSize - 1);
        }
        for (int p = 0, f = 0; f < frequency; f++)
        {
            for (int i = 0; i < items.Length; i++, p++)
            {
                Transform item = Instantiate(items[i]) as Transform;
                Vector3 position = spline.GetPoint(p * stepSize);
                item.transform.localPosition = position;

                if (lookForward)
                {
                    item.transform.LookAt(position + spline.GetDirection(p * stepSize));
                }
                item.transform.parent = transform;
                poss.Add(position);
            }
        }

    }



}
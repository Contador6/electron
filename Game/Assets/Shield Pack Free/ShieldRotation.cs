﻿using UnityEngine;
using System.Collections;

public class ShieldRotation : MonoBehaviour {

    private float rotationSpeed = 120.0f;
	
	void Update () {
        transform.Rotate(new Vector3(0f, 1f, 0f) * Time.deltaTime * this.rotationSpeed);

    }

}

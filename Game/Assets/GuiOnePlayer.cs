﻿using UnityEngine;
using System.Collections;

public class GuiOnePlayer : MonoBehaviour {

    public OnePlayer walker;

    private GUIStyle style1 = null;
    private GUIStyle style2 = null;
    private GUIStyle style3 = null;
    private GUIStyle style4 = null;
    private GUIStyle style5 = null;
    private Texture2D texture;
    public TextAsset imageAsset;
    public static float blue = 1.0f;
    public static float red = 0.0f;

    private void Start()
    {

    }

    private void InitStyles()
    {
        if (style1 == null || style2 == null || style3 == null || style4 == null || style5 == null)
        {
            style1 = new GUIStyle(GUI.skin.box);
            style1.normal.background = MakeTex(2, 2, new Color(1.0f, 0.20f, 0.0f, 0.5f));
            style2 = new GUIStyle(GUI.skin.box);
            style2.normal.background = MakeTex(2, 2, new Color(1.0f, 0.20f, 0.0f, 0.9f));
            style3 = new GUIStyle(GUI.skin.box);
            style3.normal.background = MakeTex(2, 2, new Color(0.0f, 0.00f, 1.0f, 0.5f));
            style4 = new GUIStyle(GUI.skin.box);
            style4.normal.background = MakeTex(2, 2, new Color(red, 0.00f, blue, 1.0f));
            style5 = new GUIStyle(GUI.skin.box);
            style5.normal.background = MakeTex(2, 2, new Color(1.0f, 0.00f, 0.0f, 1.0f));
        }
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    void OnGUI()
    {
        InitStyles();
        //GUI.Box(new Rect(0, 0, 100, 100), "Hello", style1);
        // Create one Group to contain both images
        // Adjust the first 2 coordinates to place it somewhere else on-screen
        style3.normal.background = MakeTex(2, 2, new Color(red, 0.00f, blue, 0.5f));
        style4.normal.background = MakeTex(2, 2, new Color(red, 0.00f, blue, 1.0f));
        GUIUtility.RotateAroundPivot(180.0f, new Vector2(Screen.width / 2, Screen.height / 2));

        GUI.BeginGroup(new Rect(Screen.width - (Screen.width-52), Screen.height - (Screen.height-32), 32, 256));
        GUI.Box(new Rect(0, 0, 32, 256), "", style3);
        GUI.BeginGroup(new Rect(0, 0, 32, walker.ChargeLevel.Count * 256 / walker.MaxChargeLevel));

        //if (walker.ChargeLevel.Count == walker.MaxChargeLevel)
        //{
        //    GUI.Box(new Rect(0, 0, 32, 256), "", style5);
        //}
        //else
            GUI.Box(new Rect(0, 0, 32, 256), "", style4);
        GUI.EndGroup();
        GUI.EndGroup();
    }
}

﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("ChargeCollectable") || collider.CompareTag("Discharger") || collider.CompareTag("LightningBolt"))
        {
            Destroy(collider.gameObject);
        }
    }
}

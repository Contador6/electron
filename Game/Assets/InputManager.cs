﻿using System.Collections.Generic;
using UnityEngine;
using System;

public static class InputManager
    {

        public static float MainHorizontal()
        {
            float r = 0.0f;
            r += Input.GetAxis("J_MainHorizontal");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }

        public static float MainVertical()
        {
            float r = 0.0f;
            r += Input.GetAxis("J_MainVertical");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }

        public static Vector3 MainJoystick()
        {
        return new Vector3(MainHorizontal(), 0, MainVertical());
        }

        public static float MainHorizontal2()
        {
            float r = 0.0f;
            r += Input.GetAxis("J_MainHorizontal2");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }

        public static float MainVertical2()
        {
            float r = 0.0f;
            r += Input.GetAxis("J_MainVertical2");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }

        public static Vector3 MainJoystick2()
        {
            return new Vector3(MainHorizontal2(), 0, MainVertical2());
        }

}

﻿using UnityEngine;
using System.Collections;

public class ObjectsDestroyer : MonoBehaviour {

    public GameObject electron;

	void Start () {
	    
	}
	
	void Update () {
        transform.position = new Vector3(electron.transform.position.x, electron.transform.position.y, electron.transform.position.z);
        transform.rotation = electron.transform.rotation;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ChargeCollectable"))
        {
            Destroy(other.gameObject);
        }
    }
}

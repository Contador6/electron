﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

    public BezierSpline spline;

    public SplineWalkerMode mode;

    public float duration;

    public bool lookForward;

    public float progress;

    public bool goingForward = true;

    void Start()
    {
        StartCoroutine(WaitBeforeStarting());
        
    }
    IEnumerator WaitBeforeStarting()
    {
        yield return new WaitForSeconds(2);
    }

    void Update()
    {
        if (goingForward)
        {
            if (OnePlayerShooter.isShooter)
                progress = OnePlayerShooter.progress-0.05f;
            else
                progress += Time.deltaTime / duration;


            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ChargeCollectable") || other.gameObject.CompareTag("LightningBolt") || other.CompareTag("Discharger")|| other.CompareTag("ActivateSpawn") || other.CompareTag("Follower"))
        {
            if (other.CompareTag("ActivateSpawn"))
            {
                EventManager.TriggerEvent("SpawnCharges");
            }
            Destroy(other.gameObject);


        }
    }

    
}

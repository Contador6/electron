﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class AbstractSpawner : MonoBehaviour {

    public BezierSpline[] splines;

    public Transform[] objectsToSpawn;

    public Transform discharger;

    public Transform portal;

    private float stepSize;

    private int size;

    private int[] arrayCharges = new int[5] { 5,6,7,8,9};
    private int[] arrayLightningBolts = new int[4] { 1,2,3,4};

    private List<int> chargesList;

    private List<int> lightningBoltsList;

    private int GenerateRandomNumberOfRange(int min, int max)
    {
       
        return UnityEngine.Random.Range(min, max);
    }

    void OnEnable()
    {
        EventManager.StartListening("SpawnDischarger", SpawnDischarger);
        EventManager.StartListening("SpawnPortal", SpawnPortal);
    }

    void OnDisable()
    {
        EventManager.StopListening("SpawnDischarger", SpawnDischarger);
        EventManager.StopListening("SpawnPortal", SpawnPortal);
    }


    public void SpawnDischarger()
    {

        Transform cloneOfobjectToSpawn;
        cloneOfobjectToSpawn = discharger;
        foreach (BezierSpline spline in splines)
        {
            size = GenerateRandomNumberOfRange(1, 3);
            stepSize = size;
            if (spline.Loop || stepSize == 1)
            {
                stepSize = 1f / stepSize;
            }
            else {
                stepSize = 1f / (stepSize - 1);
            }
            for (float p = GenerateRandomNumberOfRange(7, 9) / 10.0f, f = 0; f < size; f++)
            {
                for (int i = 0; i < 1; i++, p++)
                {
                    Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                    Vector3 position = spline.GetPoint(p * stepSize);
                    item.transform.localPosition = position;
                    item.transform.parent = transform;
                }
            }
        }
    }

    private void SpawnChargesFirstHalf()
    {
        chargesList.Clear();
        chargesList.AddRange(arrayCharges);
        Transform cloneOfobjectToSpawn = objectsToSpawn[1];
        int index;
        foreach (BezierSpline spline in splines)
        {
           index = GenerateRandomNumberOfRange(0,chargesList.Count);
           size = chargesList[index];
           stepSize = size;
           chargesList.RemoveAt(index);
                if (spline.Loop || stepSize == 1)
                {
                stepSize = 1f / stepSize;
                }
                else {
                    stepSize = 1f / (stepSize - 1);
                }
                for (float p = GenerateRandomNumberOfRange(2, 5) / 10.0f, f = 0; f < size/2; f++)
                {
                    for (int i = 0; i < 1; i++, p++)
                    {
                        Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                        Vector3 position = spline.GetPoint(p * stepSize);
                        item.transform.localPosition = position;
                        item.transform.parent = transform;
                    }
                }
        }
    }

    private void SpawnChargesSecondHalf()
    {
        chargesList.Clear();
        chargesList.AddRange(arrayCharges);
        Transform cloneOfobjectToSpawn = objectsToSpawn[1];
        int index;
        foreach (BezierSpline spline in splines)
        {
            index = GenerateRandomNumberOfRange(0, chargesList.Count);
            size = chargesList[index];
            stepSize = size;
            chargesList.RemoveAt(index);
            if (spline.Loop || stepSize == 1)
            {
                stepSize = 1f / stepSize;
            }
            else {
                stepSize = 1f / (stepSize - 1);
            }
            for (float p = size/2, f = 0; f < size; f++)
            {
                for (int i = 0; i < 1; i++, p++)
                {
                    Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                    Vector3 position = spline.GetPoint(p * stepSize);
                    item.transform.localPosition = position;
                    item.transform.parent = transform;
                }
            }
        }
    }

    private void SpawnLightningBoltsFirstHalf()
    {  
        lightningBoltsList.Clear();
        lightningBoltsList.AddRange(arrayLightningBolts);
        Transform cloneOfobjectToSpawn = objectsToSpawn[0];
        int index;
        foreach (BezierSpline spline in splines)
        {
            index = GenerateRandomNumberOfRange(0, lightningBoltsList.Count);
            size = lightningBoltsList[index];
            stepSize = size;
            lightningBoltsList.RemoveAt(index);
            if (spline.Loop || stepSize == 1)
            {
                stepSize = 1f / stepSize;
            }
            else {
                stepSize = 1f / (stepSize - 1);
            }
            for (float p = GenerateRandomNumberOfRange(6, 8) / 10.0f, f = 0; f < size/2; f++)
            {
                for (int i = 0; i < 1; i++, p++)
                {
                    Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                    Vector3 position = spline.GetPoint(p * stepSize);
                    item.transform.localPosition = position;
                    item.transform.parent = transform;
                }
            }
        }
    }

    private void SpawnLightningBoltsSecondHalf()
    {    
        lightningBoltsList.Clear();
        lightningBoltsList.AddRange(arrayLightningBolts);
        Transform cloneOfobjectToSpawn = objectsToSpawn[0];
        int index;
        foreach (BezierSpline spline in splines)
        {
            index = GenerateRandomNumberOfRange(0, lightningBoltsList.Count);
            size = lightningBoltsList[index];
            stepSize = size;
            lightningBoltsList.RemoveAt(index);
            if (spline.Loop || stepSize == 1)
            {
                stepSize = 1f / stepSize;
            }
            else {
                stepSize = 1f / (stepSize - 1);
            }
            for (float p = size/2, f = 0; f < size-1; f++)
            {
                for (int i = 0; i < 1; i++, p++)
                {
                    Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                    Vector3 position = spline.GetPoint(p * stepSize);
                    item.transform.localPosition = position;
                    item.transform.parent = transform;
                }
            }
        }
    }

    public void SpawnPortal()
    {

        Transform cloneOfobjectToSpawn;
        cloneOfobjectToSpawn = portal;
        foreach (BezierSpline spline in splines)
        {
            size = GenerateRandomNumberOfRange(1, 2);
            stepSize = size;
            if (spline.Loop || stepSize == 1)
            {
                stepSize = 1f / stepSize;
            }
            else
            {
                stepSize = 1f / (stepSize - 1);
            }
            for (float p = GenerateRandomNumberOfRange(7, 9) / 10.0f, f = 0; f < size; f++)
            {
                for (int i = 0; i < 1; i++, p++)
                {
                    Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;
                    Vector3 position = spline.GetPoint(p * stepSize);
                    item.transform.localPosition = position;
                    item.transform.parent = transform;
                }
            }
        }
    }

    void Start()
    {
        lightningBoltsList = new List<int>();
        chargesList = new List<int>();
       // SpawnPortal();
        SpawnLightningBoltsFirstHalf();
        SpawnChargesFirstHalf();

    }

    void Update()
    {
        if (OnePlayer.firstHalf)
        {
            OnePlayer.firstHalf = false;
            SpawnLightningBoltsFirstHalf();
            SpawnChargesFirstHalf();
        }
        if (OnePlayer.secondHalf)
        {
            OnePlayer.secondHalf = false;
            SpawnLightningBoltsSecondHalf();
            SpawnChargesSecondHalf();
        }

    }

}

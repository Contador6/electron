﻿using UnityEngine;
using System.Collections;

public class MarkerWalker2 : MonoBehaviour {

    public GameObject electron;

    private GameObject myLine;

    public Transform item;

    private Transform it;

    public BezierSpline[] splines;

    private BezierSpline spline;

    public float duration;

    public bool lookForward;

    public SplineWalkerMode mode;

    private float progress;
    private bool goingForward = true;

    float horizontal = 0.0f, vertical = 0.0f;

    public static int location
    {
        get;
        private set;
    }

    private void Start()
    {
        location = 2;
        spline = splines[location];
        it = Instantiate(item) as Transform;
        myLine = new GameObject();
        myLine.AddComponent<LineRenderer>();
    }

    private void DrawLine(Vector3 start, Vector3 end)
    {
        Color color = new Color32(254, 0, 0, 255);
        myLine.transform.position = start;

        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Additive"));
        lr.SetColors(color, color);
        lr.SetWidth(0.05f, 0.05f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

    private void Update()
    {
        if (SplineWalker2.IsDestroyed == true || electron.Equals(null))
            Destroy(this);

        spline = splines[location];
        it.transform.position = transform.position;
        it.transform.rotation = transform.rotation;
        it.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

        DrawLine(transform.position, electron.transform.position);

        horizontal = InputManager.MainHorizontal2();
        vertical = InputManager.MainVertical2();


        if (location == SplineWalker2.location)
        {
            it.gameObject.SetActive(false);
        }
        else if (location != SplineWalker2.location)
        {
            it.gameObject.SetActive(true);
        }

        if (horizontal <= -1.0)
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(0, -CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }

        else if (horizontal >= 1.0)
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(0, CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }

        if ((horizontal > -0.95 && horizontal < -0.05) && (vertical < 0.95 && vertical > 0.05))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(-1, -CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }
        else if ((horizontal < 0.95 && horizontal > 0.05) && (vertical < 0.95 && vertical > 0.05))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(-1, CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }
        else if ((horizontal < 0.95 && horizontal > 0.05) && (vertical > -0.95 && vertical < -0.05))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(1, CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }
        else if ((horizontal > -0.95 && horizontal < -0.05) && (vertical > -0.95 && vertical < -0.05))
        {   
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(1, -CoordinatesManager.getInversion(SplineWalker2.location%2)));
            spline = splines[location];
        }

        if (vertical <= -1.0)
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(1, 0));
            spline = splines[location];
        }

        else if (vertical >= 1.0)
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(SplineWalker2.location), new Vector2(-1, 0));
            spline = splines[location];
        }



        if (goingForward)
        {
            progress += Time.deltaTime / duration;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    private float getDistance(Vector2 p1, Vector2 p2)
    {
        return Mathf.Sqrt(Mathf.Pow((p2.x - p1.x), 2) + Mathf.Pow((p2.y - p1.y), 2));
    }

    private float getDistanceX(Vector2 p1, Vector2 p2)
    {
        return Mathf.Sqrt(Mathf.Pow((p2.x - p1.x), 2));
    }

    private float getDistanceZ(Vector2 p1, Vector2 p2)
    {
        return Mathf.Sqrt(Mathf.Pow((p2.y - p1.y), 2));
    }

    private Vector2 getPoint(Vector3 p)
    {
        return new Vector2(p.x, p.z);
    }
}

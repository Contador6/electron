﻿using UnityEngine;
using System.Collections;

public class DestroyerShooter : MonoBehaviour {

    public BezierSpline spline;

    public SplineWalkerMode mode;

    public float duration;

    public bool lookForward;

    private float progress;

    public bool goingForward = true;


    void Start()
    {
        DeactivateDestroyerShooter();
    }

    void OnEnable()
    {
        EventManager.StartListening("ActivateDestroyerShooter", ActivateDestroyerShooter);
        EventManager.StartListening("DeactivateDestroyerShooter", DeactivateDestroyerShooter);
    }

    public void ActivateDestroyerShooter()
    {
        GetComponent<BoxCollider>().enabled = true;
        GetComponent<BoxCollider>().isTrigger = true;
    }

    public void DeactivateDestroyerShooter()
    {
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<BoxCollider>().isTrigger = false;
    }


    void Update()
    {
        if (goingForward)
        {
            progress = OnePlayerShooter.progress + 0.05f;
                if (progress > 1f)
                {
                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }    
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ChargeCollectable") || other.gameObject.CompareTag("LightningBolt") || other.CompareTag("Discharger"))
        {
            Destroy(other.gameObject);
        }

        else if(other.CompareTag("ActivateSpawn"))
        {
            EventManager.TriggerEvent("SpawnCharges");
            Destroy(other.gameObject);
        }
    }
}

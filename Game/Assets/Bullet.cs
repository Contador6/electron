﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public OnePlayer player;

    public BezierSpline[] splines;

    private BezierSpline spline;

    public SplineWalkerMode mode;

    public float duration;

    private bool goingForward = true;

    private float progress;

    public bool lookForward;

    public void Start()
    {
        spline = splines[3];
        GameObject.FindGameObjectWithTag("BulletFlame").GetComponent<ParticleSystem>().enableEmission = false;
    }

    public void Update()
    {
        spline = splines[OnePlayer.location];

        if (goingForward)
        {
            if (OnePlayer.shoot == true)
            {
                progress = player.progress;
            }

            else if (OnePlayer.shoot == false)
                progress += (Time.deltaTime / duration) * 4;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;

        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("ChargeCollectable") || collider.CompareTag("Discharger") || collider.CompareTag("LightningBolt"))
        {
            Destroy(collider.gameObject);
            GameObject.FindGameObjectWithTag("Bullet").GetComponent<BoxCollider>().enabled = false;
            GameObject.FindGameObjectWithTag("BulletFlame").GetComponent<ParticleSystem>().enableEmission = false;
            OnePlayer.shoot = true;
        }
    }
}

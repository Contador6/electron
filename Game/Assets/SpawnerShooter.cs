﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerShooter : MonoBehaviour {

    public BezierSpline[] splines;

    public BezierSpline mainSpline;

    public Chargee objectToSpawn;

    public Transform objectToActivateSpawner;

    public Transform objectToConfirm;

    public Follower objectToFollow;

    public OnePlayerShooter player;

    private float []positionsCharges= new float[5] {0.01f,0.02f,0.03f,0.04f,0.05f};

    private float lastPositionOfCharges;
    private float lastPositionOfActivatorShield;

    private int GenerateRandomNumberOfRange(int min, int max)
    {

        return UnityEngine.Random.Range(min, max);
    }

    public void SpawnCharges()
    {
        List<float> list = new List<float>();
        list.AddRange(positionsCharges);
        float extraDistance;
        int isAppear=GenerateRandomNumberOfRange(1,5);
        Chargee cloneOfobjectToSpawn =objectToSpawn;
        float distance,result;
        if (isAppear > 2)
            distance = GenerateRandomNumberOfRange(12, 15) / 100.0f;
        else
            distance = GenerateRandomNumberOfRange(10, 12) / 100.0f;

        for (int i=0;i<splines.Length&&isAppear>0;i++,isAppear--)
        {
            Chargee item = Instantiate(cloneOfobjectToSpawn) as Chargee;
            extraDistance = list[GenerateRandomNumberOfRange(0, list.Count)];
            list.Remove(extraDistance);
            result= OnePlayerShooter.progress + extraDistance+distance;
            if (OnePlayerShooter.typeOfBonus == 3)
                result = OnePlayerShooter.progress - distance - extraDistance;

            if (result >= 1.0f)
                result -= 1.0f;
            else if (result < 0.0f)
                result += 1.0f;
            Vector3 position = splines[i].GetPoint(result);
            item.transform.localPosition = position;
            item.transform.parent = transform;
            item.location = i;
        }
        list.Clear();
    }

    public void ConfirmFullCharges()
    {
        Transform cloneOfobjectToSpawn = objectToConfirm;
        Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;

        float distance;

        if (OnePlayerShooter.typeOfBonus == 3)
            distance = OnePlayerShooter.progress - 0.02f;
        else
            distance = OnePlayerShooter.progress + 0.01f;

        Vector3 position = splines[OnePlayerShooter.location].GetPoint(distance);
        item.transform.localPosition = position;
        item.transform.parent = transform;
    }

    public void SpawnObjectToRandomCharges()
    {
        Transform cloneOfobjectToSpawn = objectToActivateSpawner;
        Transform item = Instantiate(cloneOfobjectToSpawn) as Transform;

        float distance;
        distance = OnePlayerShooter.progress + GenerateRandomNumberOfRange(8, 14) / 100.0f;

        if(OnePlayerShooter.typeOfBonus==3)
        {
            distance = OnePlayerShooter.progress - GenerateRandomNumberOfRange(8, 14) / 100.0f;
        }


        if (distance >= 1.0f)
            distance -= 1.0f;

        else if (distance < 0.0f)
            distance += 1.0f;

        Vector3 position = mainSpline.GetPoint(distance);
        
        
        item.transform.localPosition = position;
        item.transform.parent = transform;
    }

    void OnEnable()
    {
        EventManager.StartListening("SpawnCharges", SpawnCharges);
        EventManager.StartListening("ConfirmFullCharges",ConfirmFullCharges);
    }

    // Use this for initialization
    void Start () {
        lastPositionOfActivatorShield = 0.0f;
        lastPositionOfCharges = 0.0f;
        SpawnObjectToRandomCharges();
    }
	
	// Update is called once per frame
	void Update () {

        if (transform.childCount <= 0)
            SpawnObjectToRandomCharges();
	
	}
}

﻿using UnityEngine;
using UnityEngine.UI;// we need this namespace in order to access UI elements within our script
using System.Collections;
using UnityEngine.SceneManagement; // neded in order to load scenes

public class ButtonManager : MonoBehaviour
{
  public void NewGame(string newLevel)
    {
        SceneManager.LoadScene(newLevel);
    }
}

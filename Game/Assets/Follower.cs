﻿using UnityEngine;
using System.Collections;

public class Follower : MonoBehaviour {

    #region Settings

    private BezierSpline spline;

    public float rotationSpeed = 99.0f;

    public bool reverse = false;

    private bool isSpline = false;
    private Renderer rend; 

    public float progress
    {
        get;
        set;
    }
    #endregion

    void Start()
    {
        rend = GetComponent<Renderer>();
        progress = OnePlayerShooter.progress+ Random.Range(30, 41) / 100.0f;
        if (progress >= 1.0f)
            progress -= 1.0f;
    }

    public void SetSpline(BezierSpline spline)
    {
        isSpline = true;
        this.spline = spline;
    }

    void Update()
    {
        

        if (this.reverse)
            transform.Rotate(new Vector3(0f, 1f, 0f) * Time.deltaTime * this.rotationSpeed);
        else
            transform.Rotate(new Vector3(0f, 1f, 0f) * Time.deltaTime * this.rotationSpeed);

        progress -= (0.001f/OnePlayerShooter.currentSpeed);
        if (progress <= 0.0f)
            progress = 1.0f;

        if(isSpline)
        {
            Vector3 position = spline.GetPoint(progress);
            transform.position = position;
        }
        
    }

    public void SetRotationSpeed(float speed)
    {
        this.rotationSpeed = speed;
    }

    public void SetReverse(bool reverse)
    {
        this.reverse = reverse;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnePlayerShooter : MonoBehaviour {

    private BezierSpline spline;

    public BezierSpline[] splines;

    public SplineWalkerMode mode; 

    public float duration;

    public bool lookForward;

    public static int typeOfBonus;

    public static int shieldPoints = 0;

    private List<float> speedOfGame = new List<float>() { 1.0f,1.2f, 1.3f };

    private string[] chargesTags = new string[] { "Ch1", "Ch2", "Ch3", "Ch4", "Ch5", "Ch6", "Ch7", "Ch8", "Ch9", "Ch10" };

    private List<GameObject> chargeInfo;

    public static float progress
    {
        get;
        private set;
    }

    private bool goingForward = true;

    public static float currentSpeed = 1.0f;

    public static bool isShooter = false;

    private List<Assets.ChargeCollectable> chargeLevel = new List<Assets.ChargeCollectable>();
    public List<Assets.ChargeCollectable> ChargeLevel
    {
        get
        {
            return chargeLevel;
        }
        set
        {
            if (this.chargeLevel == null)
            {
                chargeLevel = new List<Assets.ChargeCollectable>();
                chargeLevel.Capacity = maxChargeLevel;
            }
            chargeLevel.Clear();
            value.ForEach(x => chargeLevel.Add(x));

        }
    }

    private List<GameObject> shieldObjects;

    public static int location
    {
        get;
        set;
    }

    public void RewardByPoints()
    {
        OnePlayer.scoredPoints += 200;
        if(shieldPoints<1)
            EventManager.TriggerEvent("ActivateDanger");
        this.chargeLevel.Clear();
        //maxChargeLevel++;
        EmptyInfoCharges();
        int result;

        do
        {
            result = UnityEngine.Random.Range(1, 4);
        } while (result == typeOfBonus);
        if(typeOfBonus==3)
        {
            ResetRotationInfoCharges();
        }
        typeOfBonus = result;

        if(typeOfBonus==1)
        {
            EventManager.TriggerEvent("ResetRotation");
            EventManager.TriggerEvent("InfoBonusSpeed");
            SpeedUp();
            EventManager.TriggerEvent("ActivateDestroyer");
            EventManager.TriggerEvent("DeactivateDestroyerShooter");
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Zmiana prędkości gry";
        }
        else if(typeOfBonus==2)
        {
            EventManager.TriggerEvent("InfoBonusRotation");
            OnePlayerShooter.currentSpeed = 1.0f;
            Time.timeScale = OnePlayerShooter.currentSpeed;
            EventManager.TriggerEvent("Rotate360");
            EventManager.TriggerEvent("ActivateDestroyer");
            EventManager.TriggerEvent("DeactivateDestroyerShooter");
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Rotacja platformy 360 st.";
        }
        else if(typeOfBonus==3)
        {
            EventManager.TriggerEvent("InfoBonusReverse");
            EventManager.TriggerEvent("ResetRotation");
            OnePlayerShooter.currentSpeed = 1.0f;
            Time.timeScale = OnePlayerShooter.currentSpeed;
            EventManager.TriggerEvent("ActivateDestroyerShooter");
            EventManager.TriggerEvent("DeactivateDestroyer");
            GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Jazda do tyłu";
            RotateInfoCharges();

        }
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
        StartCoroutine(turnOffInfo());

    }

    private void SpeedUp()
    {
        float result; 
        do
        {
            result = speedOfGame[UnityEngine.Random.Range(0, speedOfGame.Count)];
        } while (result == OnePlayerShooter.currentSpeed);
        OnePlayerShooter.currentSpeed = result;
        Time.timeScale = OnePlayerShooter.currentSpeed;
    }

    public void JustEmptyCharges()
    {
        if(shieldPoints<1)
            EventManager.TriggerEvent("ActivateDanger");
       
        this.chargeLevel.Clear();
    }

    private int maxChargeLevel = 10;
    public int MaxChargeLevel
    {
        get
        {
            return maxChargeLevel;
        }
    }
    public void IncreaseChargeLevel()
    {
        
        chargeLevel.Add(new Assets.ChargeCollectable());
        
        EventManager.TriggerEvent("DeactivateDanger");
            if (chargeLevel.Count >= maxChargeLevel)
            {
                EventManager.TriggerEvent("ConfirmFullCharges");
            }
        chargeInfo[chargeLevel.Count-1].GetComponent<Renderer>().enabled = true;
    }

    public void AddShield()
    {
        shieldObjects[OnePlayerShooter.shieldPoints - 1].GetComponent<Renderer>().enabled = true;
    }

    void OnEnable()
    {
        EventManager.StartListening("IncreaseChargeLevel", IncreaseChargeLevel);
        EventManager.StartListening("JustEmptyCharges", JustEmptyCharges);
        EventManager.StartListening("RewardByPoints", RewardByPoints);
        EventManager.StartListening("AddShield", AddShield);
        EventManager.StartListening("ChangeTrack", ChangeTrack);
        EventManager.StartListening("EmptyInfoCharges", EmptyInfoCharges);
        EventManager.StartListening("FailureInform",FailureInform);
        EventManager.StartListening("NoReactionInfo", NoReactionInfo);
    }

    public void ChangeTrack()
    {
        spline = splines[location];
       
    }


    // Use this for initialization
    void Start () {
        progress = 0.0f;
        Time.timeScale = currentSpeed;
        typeOfBonus = 0;
        shieldObjects = new List<GameObject>(GameObject.FindGameObjectsWithTag("Shield"));
        isShooter = true;
        foreach(GameObject shield in shieldObjects)
        {
            shield.GetComponent<Renderer>().enabled = false;
        }
        for(int i=0;i<2;i++)
        {
            OnePlayerShooter.shieldPoints++;
            EventManager.TriggerEvent("AddShield");
            EventManager.TriggerEvent("DeactivateDanger");
            GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = true;
            GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;
        }

        location = 3;
        spline = splines[location];
        GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;


        chargeInfo = new List<GameObject>();


        foreach (string tag in chargesTags)
        {
            chargeInfo.Add(GameObject.FindGameObjectWithTag(tag));
        }

    }
	
	// Update is called once per frame
	void Update () {
        QTTriggerShooter.StartQTE = true;

        if(chargeLevel.Count>0)
        {
            GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = true;
            GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = false;
        }

        if (goingForward)
        {
            if(typeOfBonus==3)
            {
                EventManager.TriggerEvent("ActivateDestroyer");
                progress -= Time.deltaTime / duration;
                 if (progress < 0.0f)
                     progress = 1.0f;
                
            }
            
            else
            {
                progress += Time.deltaTime / duration;
                if (progress > 1f)
                {
                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }
            }
         

            if (Input.GetKeyDown("escape"))
            {
                if (ChargeLevel.Count == MaxChargeLevel)
                    OnePlayer.scoredPoints += 100;
                OnePlayer.scoredPoints += (ChargeLevel.Count * 10);
                OnePlayer.scoredPoints = 0;
                QTTriggerShooter.StartQTE = false;
                QTTriggerShooter.disableSteering = false;
                OnePlayerShooter.typeOfBonus = 0;
                SceneManager.LoadScene("Menu 3D");
            }

        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }
        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ChargeCollectable"))
        {
            Destroy(other.gameObject);
            try
            {
                if(OnePlayerShooter.shieldPoints>=1)
                {
                    shieldObjects[OnePlayerShooter.shieldPoints - 1].GetComponent<Renderer>().enabled = false;
                    OnePlayerShooter.shieldPoints--;
                    if (chargeLevel.Count == 0 && shieldPoints == 0)
                    {
                        EventManager.TriggerEvent("ActivateDanger");
                        GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = false;
                        GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = true;
                    }
                        
                    return;
                }

                chargeLevel.RemoveAt((chargeLevel.Count - 1));
                chargeInfo[chargeLevel.Count].GetComponent<Renderer>().enabled = false;
                if (chargeLevel.Count == 0&&shieldPoints==0)
                {
                    EventManager.TriggerEvent("ActivateDanger");
                    GameObject.FindGameObjectWithTag("PlayerBlueFlame").GetComponent<ParticleSystem>().enableEmission = false;
                    GameObject.FindGameObjectWithTag("PlayerRedFlame").GetComponent<ParticleSystem>().enableEmission = true;
                }
                    
                
            }
            catch(ArgumentOutOfRangeException e)
            {
                Destroy(other.gameObject);
                QTTriggerShooter.StartQTE = false;
                QTTriggerShooter.disableSteering = false;
                Destroy(this);
                return;
            }
        }

        else if (other.gameObject.CompareTag("ActivateSpawn"))
        {
            EventManager.TriggerEvent("SpawnCharges");
            Destroy(other.gameObject);
        }

    }

    void OnDestroy()
    {
        QTTriggerShooter.StartQTE = false;
        QTTriggerShooter.disableSteering = false;
        if (ChargeLevel.Count == MaxChargeLevel)
            OnePlayer.scoredPoints += 100;
        OnePlayer.scoredPoints += (ChargeLevel.Count * 10);
        OnePlayerShooter.typeOfBonus = 0;
        SceneManager.LoadScene("GameOver");
    }


    public void EmptyInfoCharges()
    {
        foreach (GameObject gg in chargeInfo)
        {
            gg.GetComponent<Renderer>().enabled = false;
        }
    }

    public void FailureInform()
    {
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Wciśnięto niepoprawny przycisk";
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
        StartCoroutine(turnOffInfo());
    }

    public void NoReactionInfo()
    {
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().text = "Nic nie wciśnięto";
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = true;
        StartCoroutine(turnOffInfo());
    }

    public static IEnumerator turnOffInfo()
    {
        yield return new WaitForSeconds(2);
        GameObject.FindGameObjectWithTag("Feedback").GetComponent<Text>().enabled = false;
    }

    public void RotateInfoCharges()
    {
        foreach(GameObject gg in chargeInfo)
        {
            gg.transform.Rotate(0.0f, 180.0f, 0.0f);
        }
    }

    public void ResetRotationInfoCharges()
    {
        foreach (GameObject gg in chargeInfo)
        {
            gg.transform.Rotate(new Vector3(0.0f, -180.0f, 0.0f));
        }
    }
}

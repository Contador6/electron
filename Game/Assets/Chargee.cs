﻿using UnityEngine;
using System.Collections;

public class Chargee : MonoBehaviour {

    public int location
    {
        get;
        set;
    }
    private Renderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Specular");
    }
	
	// Update is called once per frame
	void Update () {

        if (location == OnePlayerShooter.location||location == MarkerWalkerShooter.location)
        {
            rend.material.SetColor("_SpecColor", Color.red);
        }
        else
        {
            rend.material.SetColor("_SpecColor", Color.yellow);
        }


        transform.Rotate(new Vector3(1f, 0f, 0f) * Time.deltaTime * 90);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeOnePlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("LightningBolt") || collider.CompareTag("Discharger") || collider.CompareTag("ChargeCollectable"))
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("LightningBolt") || collider.CompareTag("Discharger") || collider.CompareTag("ChargeCollectable"))
        {
            Destroy(this.gameObject);
        }
    }
}

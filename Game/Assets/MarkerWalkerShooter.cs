﻿using UnityEngine;
using System.Collections;

public class MarkerWalkerShooter : MonoBehaviour {

    public OnePlayerShooter player;

    public GameObject electron;

    public Transform item;

    public BezierSpline[] splines;

    private static BezierSpline spline;

    private Transform lineItem;

    public SplineWalkerMode mode;

    public float duration;

    public bool lookForward;

    public static float progress
    {
        get;
        private set;
    }
    private bool goingForward = true;

    private float horizontal;

    private float vertical;

    public static int location
    {
        get;
        private set;
    }

    private float timer = 6.0f;

    private float secondTimer = 0.0f;

    private int choice;

    private void Start()
    {
        location = 3;
        spline = splines[location];
        lineItem = Instantiate(item) as Transform;
        timer = Random.Range(5,7);
        secondTimer = timer;

    }


    private void Update()
    {
        timer -= Time.deltaTime;
        spline = splines[location];
        lineItem.transform.position = transform.position;
        lineItem.transform.rotation = transform.rotation;
        lineItem.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

        

        horizontal = InputManager.MainHorizontal();
        vertical = InputManager.MainVertical();

        if (location == OnePlayerShooter.location)
        {
            lineItem.gameObject.SetActive(false);
        }
        else if (location != OnePlayerShooter.location)
        {
            lineItem.gameObject.SetActive(true);
        }

        if (secondTimer - timer > 1.5f)
        {
            OnePlayerShooter.location = location;
            EventManager.TriggerEvent("ChangeTrack");

        }

        if(timer<=0)
        {
            choice = Random.Range(0, 4);
            timer = Random.Range(5, 7);
        }
        

        if (choice==0)
        {
            spline = splines[choice];
            location = choice;
        }

        else if (choice==1)
        {
            spline = splines[choice];
            location = choice;
        }

        else if (choice == 2)
        {
            spline = splines[choice];
            location = choice;
        }
        else if(choice==3)
        {
            spline = splines[choice];
            location = choice;
        }

        if (goingForward)
        {

            progress = OnePlayerShooter.progress;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }

       

    }
}

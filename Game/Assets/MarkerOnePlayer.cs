﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class MarkerOnePlayer : MonoBehaviour {

    public OnePlayer player;

    public GameObject electron;

    private GameObject myLine;

    public Transform item;

    public BezierSpline[] splines;

    private static BezierSpline spline;

    private Transform lineItem;

    public SplineWalkerMode mode;

    public float duration;

    public bool lookForward;

    public static float progress
    {
        get;
        private set;
    }
    private bool goingForward = true;

    private float horizontal;

    private float vertical;

    public static int location
    {
        get;
        private set;
    }

    private void Start()
    {
        location = 3;
        spline = splines[location];
        lineItem = Instantiate(item) as Transform;
        myLine = new GameObject();
        myLine.AddComponent<LineRenderer>();
        horizontal = 0.0f;
        vertical = 0.0f;

    }

    private void DrawLine(Vector3 start, Vector3 end)
    {
        Color color = new Color32(255, 255, 255, 255);
        myLine.transform.position = start;

        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Additive"));
        lr.SetColors(color, color);
        lr.SetWidth(0.05f, 0.05f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

    private void Update()
    {
        spline = splines[location];
        lineItem.transform.position = transform.position;
        lineItem.transform.rotation = transform.rotation;
        lineItem.transform.Rotate(new Vector3(90.0f, 0.0f, 0.0f));

        DrawLine(transform.position, electron.transform.position);

        horizontal = InputManager.MainHorizontal();
        vertical = InputManager.MainVertical();

        if (location == OnePlayer.location)
        {
            lineItem.gameObject.SetActive(false);
        }
        else if (location != OnePlayer.location)
        {
            lineItem.gameObject.SetActive(true);
        }

        if ((horizontal <= -1.0))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(0, -CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }

        else if ((horizontal >= 1.0))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(0, CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }

        if (((horizontal > -0.95 && horizontal < -0.05) && (vertical < 0.95 && vertical > 0.05)))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(-1, -CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }
        else if (((horizontal < 0.95 && horizontal > 0.05) && (vertical < 0.95 && vertical > 0.05)))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(-1, CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }
        else if (((horizontal < 0.95 && horizontal > 0.05) && (vertical > -0.95 && vertical < -0.05)))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(1, CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }
        else if (((horizontal > -0.95 && horizontal < -0.05) && (vertical > -0.95 && vertical < -0.05)))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(1, -CoordinatesManager.getInversion(OnePlayer.location % 2)));
            spline = splines[location];
        }

        if ((vertical <= -1.0))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(1, 0));
            spline = splines[location];
        }

        else if ((vertical >= 1.0))
        {
            location = CoordinatesManager.checkMovement(CoordinatesManager.checkCoordinates(OnePlayer.location), new Vector2(-1, 0));
            spline = splines[location];
        }

        if (goingForward)
        {

            progress = player.progress;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }

    }


}

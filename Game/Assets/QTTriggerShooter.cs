﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class QTTriggerShooter : MonoBehaviour {

    private bool pressedButton = false;
    public static bool StartQTE;
    public static bool disableSteering;

    private bool fill;
    private float currentAmount;
    private float speed = 1000;

    private Canvas canvas;
    private Text text;

    public float CountTimer = 0.05f;

    public enum QTState { Ready, Ongoing, Done };
    public QTState qtState = QTState.Ready;

    public enum QTResponse { Null, Success, Fail };
    public QTResponse qtResponse = QTResponse.Null;

    private string bonusInformation;

    public KeyCode[] options;

    public string PlayerTag = "Player";

    public Dictionary<KeyCode, string> KeyText;

    private KeyCode randomizedButton;

    private void Awake()
    {
        options = new KeyCode[4]
        {
            /*KeyCode.A,
            KeyCode.B,
            KeyCode.X,
            KeyCode.Y*/
            KeyCode.JoystickButton0,
            KeyCode.JoystickButton1,
            KeyCode.JoystickButton2,
            KeyCode.JoystickButton3
        };
        KeyText = new Dictionary<KeyCode, string>();
        KeyText.Add(options[0], "A");
        KeyText.Add(options[1], "B");
        KeyText.Add(options[2], "X");
        KeyText.Add(options[3], "Y");
        canvas = GameObject.Find("QTE_Canvas").GetComponent<Canvas>();
        text = canvas.GetComponentInChildren<Text>();
    }

    private void Start()
    {
        StartQTE = false;
        fill = false;
        disableSteering = false;
    }

    private void randomizeButton()
    {
        int r;
        System.Random random = new System.Random();
        r = random.Next(0, options.Length);
        randomizedButton = options[r];
    }

    void OnEnable()
    {
        EventManager.StartListening("InfoBonusSpeed", InfoBonusSpeed);
        EventManager.StartListening("InfoBonusRotation", InfoBonusRotation);
        EventManager.StartListening("InfoBonusReverse", InfoBonusReverse);
    }

    public void InfoBonusSpeed()
    {
        bonusInformation = "Bonus Speed";
    }

    public void InfoBonusRotation()
    {
        bonusInformation = "Bonus Rotation 360";
    }

    public void InfoBonusReverse()
    {
        bonusInformation = "Ride to end";
    }

    public void ShowInfoBonus()
    {
        text.fontSize = 40;
        text.text = bonusInformation;
    }

    public void turnPanelOn()
    {
        canvas.enabled = true;
    }

    public void turnPanelOff()
    {
        canvas.enabled = false;
    }

    public void changeText(KeyCode code)
    {
        text.text = KeyText[code];
    }

    public IEnumerator ShowInfo()
    {
        EventManager.TriggerEvent("RewardByPoints");
        ShowInfoBonus();
        turnPanelOn();
        yield return new WaitForSeconds(CountTimer);
        turnPanelOff();
        Time.timeScale = OnePlayerShooter.currentSpeed;
        Debug.Log("dobrze wcisnąłem");
        Destroy(this.gameObject);
        disableSteering = false;
    }

    private void Update()
    {
        if (fill)
        {
            if (currentAmount > 0)
            {
                currentAmount -= speed * Time.deltaTime;
            }
            //timerImage.fillAmount = currentAmount / 100;
            text.fontSize = (int)currentAmount;
        }
        if (StartQTE)
        {
            if (qtState == QTState.Ongoing)
            {
                if (Input.GetKeyDown(randomizedButton))
                {
                    pressedButton = true;
                    qtState = QTState.Done;
                    qtResponse = QTResponse.Success;
                    StopCoroutine(StateChange());
                    StartCoroutine(ShowInfo());
                }
                else if (Input.anyKeyDown)
                {
                    pressedButton = true;
                    qtState = QTState.Done;
                    qtResponse = QTResponse.Fail;
                    StopCoroutine(StateChange());
                    turnPanelOff();
                    Time.timeScale = OnePlayerShooter.currentSpeed;
                    Debug.Log("źle wcisnąłem");
                    EventManager.TriggerEvent("FailureInform");
                    Destroy(this.gameObject);
                    disableSteering = false;
                }

                int indexChildren = GameObject.Find("Spawner").transform.childCount;
                for (int i = 0; i < indexChildren; i++)
                {
                    if (!GameObject.FindGameObjectWithTag("Spawner").transform.GetChild(i).gameObject.CompareTag("LightningBolt"))
                        Destroy(GameObject.FindGameObjectWithTag("Spawner").transform.GetChild(i).gameObject);
                }

            }
        }
        if (qtResponse == QTResponse.Fail && qtState == QTState.Done)
        {
            EventManager.TriggerEvent("JustEmptyCharges");
            EventManager.TriggerEvent("EmptyInfoCharges");
        }
            
    }

    void OnTriggerEnter(Collider other)
    {
        if (StartQTE)
        {
            randomizeButton();
            if (qtState == QTState.Ready && other.tag == PlayerTag)
            {
                Debug.Log("Wlazłem w piorun");
                Time.timeScale = 0.1f;
                disableSteering = true;
                StartCoroutine(StateChange());
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (StartQTE)
        {
            if (other.tag == PlayerTag)
            {
                turnPanelOff();
                StopCoroutine(StateChange());
                Time.timeScale = OnePlayerShooter.currentSpeed;
                qtState = QTState.Ready;
                disableSteering = false;
                fill = false;
                if (!pressedButton)
                {
                    EventManager.TriggerEvent("JustEmptyCharges");
                    EventManager.TriggerEvent("EmptyInfoCharges");
                    EventManager.TriggerEvent("NoReactionInfo");
                }
                    
                Destroy(this.gameObject);
            }
            pressedButton = false;
        }
    }

    private IEnumerator StateChange()
    {
        changeText(randomizedButton);
        currentAmount = 150;
        fill = true;
        turnPanelOn();
        qtState = QTState.Ongoing;
        yield return new WaitForSeconds(CountTimer);
        // If the timer is over and the event isn't over? Fix it! because most likely they failed.
        turnPanelOff();
        Time.timeScale = OnePlayerShooter.currentSpeed;
        disableSteering = false;
        fill = false;
        qtState = QTState.Done;
        qtResponse = QTResponse.Fail;
        Destroy(this.gameObject);
    }
}

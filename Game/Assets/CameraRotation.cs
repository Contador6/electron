﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {

    public Transform electron;

    public Transform target;

    public float distance;

    public float height;

    public float heightDamping;

    public float rotationDamping;

    private float wantedHeight;

    private float wantedRotationAngleY;

    private float currentRotationAngleY;

    private float wantedRotationAngleX;

    private float currentRotationAngleX;

    private float wantedRotationAngleZ;

    private float currentRotationAngleZ;

    private float currentHeight;

    private float horizontal;

    private float vertical;

    private float rotation;

    private Quaternion currentRotation;

    void OnEnable()
    {
        EventManager.StartListening("Rotate360", Rotate360);
        EventManager.StartListening("ResetRotation",ResetRotation);
    }

    public void Rotate360()
    {
        rotation = 14.0f;
    }

    public void ResetRotation()
    {
        rotation = 8.0f;
    }

    public void Start()
    {
        rotation = 8.0f;
    }

    void LateUpdate()
    {
        if (!target)
            return;


        horizontal = InputManager.MainHorizontal();
        vertical = InputManager.MainVertical();


        wantedHeight = target.position.y + height;

        // Calculate the current rotation angles
        wantedRotationAngleY = target.eulerAngles.y;


        if (QTTriggerShooter.disableSteering)
        {
            currentRotationAngleY = transform.eulerAngles.y;
        }
            
        else
        {
            currentRotationAngleY = transform.eulerAngles.y + horizontal * rotation;
            currentRotationAngleZ = transform.eulerAngles.z + vertical * rotation;
        }
            


        currentHeight = transform.position.y;
        currentRotationAngleY = Mathf.LerpAngle(currentRotationAngleY, wantedRotationAngleY, rotationDamping * Time.deltaTime);


        wantedRotationAngleX = target.eulerAngles.x;
        currentRotationAngleX = transform.eulerAngles.x;
        currentRotationAngleX = Mathf.LerpAngle(currentRotationAngleX, wantedRotationAngleX, rotationDamping * Time.deltaTime);

        wantedRotationAngleZ = target.eulerAngles.z;
        currentRotationAngleZ = transform.eulerAngles.z;
 
        currentRotationAngleZ = Mathf.LerpAngle(currentRotationAngleZ, wantedRotationAngleZ, rotationDamping * Time.deltaTime);




        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

         /*if (QTTriggerShooter.disableSteering)
             target.localPosition = new Vector3(target.position.x, target.position.y, target.position.z);
         else
             target.localPosition = new Vector3(target.position.x, target.position.y + vertical * 5.0f, target.position.z);*/


        // Convert the angle into a rotation
        currentRotation = Quaternion.Euler(currentRotationAngleX, currentRotationAngleY, currentRotationAngleZ);

        

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        transform.position = target.position;
        transform.position -= currentRotation * Vector3.forward * distance;

        // Set the height of the camera
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
        // Always look at the target
        
        transform.LookAt(target);
        if(!QTTriggerShooter.disableSteering)
            transform.Rotate((-1)*vertical*50, 0.0f, 0.0f);



    }
}

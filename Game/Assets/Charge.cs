﻿using UnityEngine;
using System.Collections;

public class Charge : MonoBehaviour {

    private Renderer rend;

    void Start () {
        rend = GetComponent<Renderer>();
    }
	
	void Update () {



    }

    public void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("LightningBolt") || collider.CompareTag("Discharger") || collider.CompareTag("ChargeCollectable"))
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("LightningBolt") || collider.CompareTag("Discharger") || collider.CompareTag("ChargeCollectable"))
        {
            Destroy(this.gameObject);
        }
    }
}

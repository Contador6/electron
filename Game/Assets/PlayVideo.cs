﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PlayVideo : MonoBehaviour {

    public MovieTexture movie;

    private AudioSource audio;

	void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        movie.loop = true;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
        movie.Play();     
        //audio.Play();
	}
	

	void Update () {
		if(!movie.isPlaying)
        {
            movie.Play();
        }
	}
}

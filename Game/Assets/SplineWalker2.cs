﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SplineWalker2 : MonoBehaviour
{
    private List<Assets.ChargeCollectable> chargeLevel = new List<Assets.ChargeCollectable>();
    public List<Assets.ChargeCollectable> ChargeLevel
    {
        get
        {
            return chargeLevel;
        }
        set
        {
            if (this.chargeLevel == null)
            {
                chargeLevel = new List<Assets.ChargeCollectable>();
                chargeLevel.Capacity = maxChargeLevel;
            }
            chargeLevel.Clear();
            value.ForEach(x => chargeLevel.Add(x));
     
        }
    }

    private int maxChargeLevel = 50;
    public int MaxChargeLevel
    {
        get
        {
            return maxChargeLevel;
        }
    }

    public BezierSpline[] splines;

    private BezierSpline spline;

    public float duration;

    private float trigger = 0.0f;

    public static int location
    {
        get;
        private set;
    }
    public BezierSpline Spline { get { return this.spline; } }

    public bool lookForward;

    public SplineWalkerMode mode;

    private float progress;
    private bool goingForward = true;
    private bool canCollectCharges=true;
    public SplineWalker friendElectorn;
    private float durationIdle;
    public static bool IsDestroyed { get; set; }
    private void Start()
    {
        location = 2;
        spline = splines[location];
        chargeLevel.Capacity = maxChargeLevel;
        GameObject.Find("Object2").GetComponent<MeshRenderer>().enabled = false;
    }
    public void IncreaseChargeLevel()
    {
        if (chargeLevel.Count == maxChargeLevel)
            throw new System.ArgumentOutOfRangeException();
        else
        {
            chargeLevel.Add(new Assets.ChargeCollectable());
            Debug.Log("Walker2 " + "Charge Level = " + this.chargeLevel.Count);
        }
    }
    public void OnTriggerEnter(Collider c)
    {
        Debug.Log("Entering OnTrigger");
        if (c.CompareTag("ChargeCollectable") && canCollectCharges)
        {
            Debug.Log("inside first if");
            try
            {
                IncreaseChargeLevel();
                transform.localScale = new Vector3(transform.localScale.x + 0.05f, transform.localScale.y + 0.05f, transform.localScale.z + 0.05f);
                StartCoroutine(AD());
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                Debug.Log("Exceeded Max Charge Level KABOOM");
                IsDestroyed = true;
                Destroy(this);
                return;
            }

            Destroy(c.gameObject);
        }
        if (c.CompareTag("LightingBolt") && this.ChargeLevel.Count != 0)
        {
            if (friendElectorn.Spline == this.spline)
            {
                this.chargeLevel.RemoveAt(this.ChargeLevel.Count - 1);
                Debug.Log("Removing one charge present Count is " + this.chargeLevel.Count);
            }
            else
            {
                Debug.Log("Killed By LightingBolt R.I.P");
                IsDestroyed = true;
                Destroy(this);

                Destroy(friendElectorn);
            }
        }
    }

    private void Update()
    {

        durationIdle -= Time.deltaTime;
        if (location == SplineWalker.location)
        {
            canCollectCharges = false;

        }
        if (location != SplineWalker.location)
        {
            canCollectCharges = true;

        }

        if (durationIdle <= 0)
        {
           
        }

        trigger = Input.GetAxis("Trigger2");

        if (trigger < 0)
        {
            spline = splines[MarkerWalker2.location];
            location = MarkerWalker2.location;
        }


        if (goingForward)
        {
            progress += Time.deltaTime / duration;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }
        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }

    private IEnumerator AD()
    {
        //Render
        GameObject.Find("Object2").GetComponent<MeshRenderer>().enabled = true;
        //Wait Before Disapearing
        yield return new WaitForSeconds(0.3f);
        //Dissapear
        GameObject.Find("Object2").GetComponent<MeshRenderer>().enabled = false;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {

    public BezierSpline spline;

    public OnePlayer player;

    public float duration;

    public SplineWalkerMode mode;

    public float progress
    {
        get;
        private set;
    }

    public bool lookForward;

    private bool goingForward = true;

    public GameObject Destroyer;
    private Object destroyer;

    void Start () {
        
        Destroyer.AddComponent<Destroyer>();
        Destroyer.GetComponent<BoxCollider>().isTrigger= true;
        Destroyer.GetComponent<BoxCollider>().size = new Vector3(11, 11,1.0f);
        Destroyer.GetComponent<Destroyer>().duration = 40;
        Destroyer.GetComponent<Destroyer>().spline = this.spline;
        Destroyer.GetComponent<Destroyer>().mode = this.mode;
        Destroyer.GetComponent<Destroyer>().lookForward = true;

        StartCoroutine(WaitBeforeStarting());

    }
    private IEnumerator WaitBeforeStarting()
    {
        yield return new WaitForSeconds(2);
        destroyer = Instantiate(Destroyer, this.transform.localPosition, this.transform.localRotation, this.transform);
        Destroyer.SetActive(true);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ChargeCollectable"))
        {
            Destroy(other.gameObject);
        }
    }

    void Update()
    {
        if (goingForward)
        {
            progress = player.progress;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraControllerShooter : MonoBehaviour {

    public BezierSpline spline;

    public Transform target;

    public float duration;

    public SplineWalkerMode mode;

    public GameObject Destroyer;
    private Object destroyer;

    private float YClamp;

    private float gg = 0.0f;

    public float progress
    {
        get;
        private set;
    }

    public bool lookForward;

    private bool goingForward = true;

    void Start()
    {
        Destroyer.AddComponent<Destroyer>();
        Destroyer.GetComponent<BoxCollider>().isTrigger = true;
        Destroyer.GetComponent<BoxCollider>().size = new Vector3(11, 11, 1.0f);
        Destroyer.GetComponent<Destroyer>().duration = 40;
        Destroyer.GetComponent<Destroyer>().spline = this.spline;
        Destroyer.GetComponent<Destroyer>().mode = this.mode;
        Destroyer.GetComponent<Destroyer>().lookForward = true;

        StartCoroutine(WaitBeforeStarting());

    }

    private IEnumerator WaitBeforeStarting()
    {
        yield return new WaitForSeconds(2);
        destroyer = Instantiate(Destroyer, this.transform.localPosition, this.transform.localRotation, this.transform);
        Destroyer.SetActive(true);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Follower")|| other.gameObject.CompareTag("ActivateSpawn"))
        {
            Destroy(other.gameObject);
        }
    }



    void Update()
    {
        if(OnePlayerShooter.typeOfBonus==3)
        {
            Destroyer.GetComponent<BoxCollider>().enabled = false;
            Destroyer.GetComponent<BoxCollider>().isTrigger = false;
        }
        else
        {
            Destroyer.GetComponent<BoxCollider>().enabled = true;
            Destroyer.GetComponent<BoxCollider>().isTrigger = true;
        }

        if (goingForward)
        {
            progress = OnePlayerShooter.progress;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }

        else {
            progress -= Time.deltaTime / duration;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;

        if (lookForward)
        {
            YClamp = transform.rotation.y;
            transform.LookAt(position + spline.GetDirection(progress));

            //transform.RotateAround(transform.localPosition, Vector3.up, CameraRotation.XPad);
        }

        if(OnePlayerShooter.typeOfBonus==3)
        {
            transform.LookAt((position + (-1)*spline.GetDirection(progress)));
        }

    }

    void OnEnable()
    {
        EventManager.StartListening("ActivateDestroyer", ActivateDestroyer);
        EventManager.StartListening("DeactivateDestroyer", DeactivateDestroyer);
    }

    public void ActivateDestroyer()
    {
        Destroyer.GetComponent<BoxCollider>().enabled = true;
        Destroyer.GetComponent<BoxCollider>().isTrigger = true;
    }

    public void DeactivateDestroyer()
    {
        Destroyer.GetComponent<BoxCollider>().enabled = false;
        Destroyer.GetComponent<BoxCollider>().isTrigger = false;
    }
}
